package tn.enig.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.enig.model.Article;

@Repository
public interface IArticle extends JpaRepository<Article, Integer> {

}
