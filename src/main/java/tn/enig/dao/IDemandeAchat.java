package tn.enig.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.enig.model.DemandeAchat;

@Repository
public interface IDemandeAchat extends JpaRepository<DemandeAchat, Integer> {

}
