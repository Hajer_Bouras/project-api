package tn.enig.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "article")
public class Article extends Object {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer code;
	private String designation;
	private double prix_u;
	private int qte;
	private Lot num_lot;
	private DemandeAchat num_DA;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name="lot_id")
	private Lot lot;

	public Article() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Article(Integer code, String designation, double prix_u, int qte, Lot num_lot, DemandeAchat num_DA) {
		super();
		this.code = code;
		this.designation = designation;
		this.prix_u = prix_u;
		this.qte = qte;
		this.num_lot = num_lot;
		this.num_DA = num_DA;
	}

	private Integer getCode() {
		return code;
	}

	private void setCode(Integer code) {
		this.code = code;
	}

	private String getDesignation() {
		return designation;
	}

	private void setDesignation(String designation) {
		this.designation = designation;
	}

	private double getPrix_u() {
		return prix_u;
	}

	private void setPrix_u(double prix_u) {
		this.prix_u = prix_u;
	}

	private int getQte() {
		return qte;
	}

	private void setQte(int qte) {
		this.qte = qte;
	}

	private Lot getNum_lot() {
		return num_lot;
	}

	private void setNum_lot(Lot num_lot) {
		this.num_lot = num_lot;
	}

	private DemandeAchat getNum_DA() {
		return num_DA;
	}

	private void setNum_DA(DemandeAchat num_DA) {
		this.num_DA = num_DA;
	}

	@Override
	public String toString() {
		return "Article [code=" + code + ", designation=" + designation + ", prix_u=" + prix_u + ", qte=" + qte
				+ ", num_lot=" + num_lot + ", num_DA=" + num_DA + "]";
	}
	
	
	
	
	
	
	

}
