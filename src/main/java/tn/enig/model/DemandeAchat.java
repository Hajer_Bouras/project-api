package tn.enig.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "DemandeAchat")
public class DemandeAchat extends Object {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer num_DA;
	private String title;
	private double budget;
	private int nb_lot;
	private Date date;
	private String nature;
	private String etat;
	
	@ManyToMany
	@JoinColumn(name="lot_id")
	private Lot lot;
	
	@ManyToMany
	@JoinColumn(name="id_user")
	private User user;
	
	private Integer getNum_DA() {
		return num_DA;
	}
	private void setNum_DA(Integer num_DA) {
		this.num_DA = num_DA;
	}
	private String getTitle() {
		return title;
	}
	private void setTitle(String title) {
		this.title = title;
	}
	private double getBudget() {
		return budget;
	}
	private void setBudget(double budget) {
		this.budget = budget;
	}
	private int getNb_lot() {
		return nb_lot;
	}
	private void setNb_lot(int nb_lot) {
		this.nb_lot = nb_lot;
	}
	private Date getDate() {
		return date;
	}
	private void setDate(Date date) {
		this.date = date;
	}
	private String getNature() {
		return nature;
	}
	private void setNature(String nature) {
		this.nature = nature;
	}
	private String getEtat() {
		return etat;
	}
	private void setEtat(String etat) {
		this.etat = etat;
	}
	
	public DemandeAchat() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DemandeAchat(Integer num_DA, String title, double budget, int nb_lot, Date date, String nature,
			String etat) {
		super();
		this.num_DA = num_DA;
		this.title = title;
		this.budget = budget;
		this.nb_lot = nb_lot;
		this.date = date;
		this.nature = nature;
		this.etat = etat;
	}
	
	
	

}
