package tn.enig.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "lot")
public class Lot extends Object {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer num_lot;
	private DemandeAchat num_DA;
	
	@OneToMany(mappedBy = "lot" , cascade=CascadeType.ALL , fetch = FetchType.LAZY)
	@JoinColumn(name="article_id")
	private List <Article> art;
	
	
	@ManyToMany
	@JoinColumn(name="DA_id")
	private DemandeAchat DA;
	
	public Lot() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Lot(Integer num_lot, DemandeAchat num_DA) {
		super();
		this.num_lot = num_lot;
		this.num_DA = num_DA;
	}

	private Integer getNum_lot() {
		return num_lot;
	}

	private void setNum_lot(Integer num_lot) {
		this.num_lot = num_lot;
	}

	private DemandeAchat getNum_DA() {
		return num_DA;
	}

	private void setNum_DA(DemandeAchat num_DA) {
		this.num_DA = num_DA;
	}

	@Override
	public String toString() {
		return "Lot [num_lot=" + num_lot + ", num_DA=" + num_DA + "]";
	}
	
}
