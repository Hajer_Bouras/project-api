package tn.enig.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User extends Object  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id_user;
	private String nom;
	private String prenom;
	private String email;
	
	@ManyToMany
	@JoinColumn(name="DA_id")
	private DemandeAchat DA;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(Integer id_user, String nom, String prenom, String email) {
		super();
		this.id_user = id_user;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
	}

	private Integer getId_user() {
		return id_user;
	}

	private void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	private String getNom() {
		return nom;
	}

	private void setNom(String nom) {
		this.nom = nom;
	}

	private String getPrenom() {
		return prenom;
	}

	private void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	private String getEmail() {
		return email;
	}

	private void setEmail(String email) {
		this.email = email;
	}	
	
	}
