package tn.enig.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.enig.dao.IArticle;
import tn.enig.model.Article;

@RestController
public class ArticleController {

	@Autowired
	IArticle art;
	
	public void setArt(IArticle art) {
		this.art = art;
	}
	
	@GetMapping("/article")
	public List<Article> get1(){
		return art.findAll();
	}
	
	@PostMapping("/addArticle")
	public void add(@RequestBody Article a) {
		art.save(a);
	}
	
	@DeleteMapping("/deleteArticle/{id}")
	public void del(@PathVariable("id") int id) {
		art.deleteById(id);
	}

}
