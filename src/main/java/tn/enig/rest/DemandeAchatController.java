package tn.enig.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.enig.dao.IDemandeAchat;
import tn.enig.model.DemandeAchat;

@RestController
public class DemandeAchatController {
	
	@Autowired
	IDemandeAchat da;
	
	public void setDa(IDemandeAchat da) {
		this.da = da;
	}
	
	@GetMapping("/DA")
	public List<DemandeAchat> get1(){
		return da.findAll();
	}
	
	@PostMapping("/addDA")
	public void add(@RequestBody DemandeAchat d) {
		da.save(d);
	}
	
	@DeleteMapping("/deleteDA/{id}")
	public void del(@PathVariable("id") int id) {
		da.deleteById(id);
	}

}
