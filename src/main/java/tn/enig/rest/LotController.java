package tn.enig.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.enig.dao.ILot;
import tn.enig.model.Lot;

@RestController
public class LotController {
	
	@Autowired
	ILot lot;
	
	public void setLot(ILot lot) {
		this.lot = lot;
	}
	
	@GetMapping("/lot")
	public List<Lot> get1(){
		return lot.findAll();
	}
	
	@PostMapping("/addLot")
	public void add(@RequestBody Lot l) {
		lot.save(l);
	}

	@DeleteMapping("/deleteLot/{id}")
	public void del(@PathVariable("id") int id) {
		lot.deleteById(id);
	}


}
