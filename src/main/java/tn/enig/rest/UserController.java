package tn.enig.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tn.enig.dao.IUser;
import tn.enig.model.User;

@RestController
public class UserController {
	
	@Autowired
	IUser user;
	
	public void setUser(IUser user) {
		this.user = user;
	}
	
	@GetMapping("/user")
	public List<User> get1(){
		return user.findAll();
	}
	
	@PostMapping("/addUser")
	public void add(@RequestBody User u) {
		user.save(u);
	}
	
	@DeleteMapping("/deleteUser/{id}")
	public void del(@PathVariable("id") int id) {
		user.deleteById(id);
	}

}
